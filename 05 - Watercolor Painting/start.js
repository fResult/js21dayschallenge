(() => {
  // const colors = ['240, 201, 201', '246, 167, 186', '254, 185, 200','242, 166, 166', '230, 136, 161', '192, 92, 126'];
  const colors = ['249,149,127', '171,207,209', '212,230,196', '255,207,203', '255,207,203', '171,190,236', '247,215,194', '140,193,211'];
  const canvas = document.querySelector('#painting');
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;

  const context = canvas.getContext('2d');

  let previousPoint = { x: 0, y: 0 };

  function getDistance(previousPoint, currentPoint) {
    return Math.sqrt((previousPoint.x - currentPoint.x) ** 2 + (previousPoint.y - currentPoint.y) ** 2);
  }

  function onMouseMove({ pageX, pageY }) {
    const currentPoint = { x: pageX, y: pageY };

    context.beginPath();

    context.lineCap = 'round';
    // context.lineJoin = 'round';

    const distance = getDistance(previousPoint, currentPoint);
    context.lineWidth = Math.random() / distance * 40;

    const opacity = 0.5 / distance;

    // context.strokeStyle = `rgba(30, 144, 255, ${opacity})`;
    // context.strokeStyle = `rgba(${colors[Math.ceil(Math.random() * 6)]}, ${opacity})`;
    context.strokeStyle = `rgba(${colors[Math.ceil(Math.random() * 10)]}, ${opacity})`;

    context.moveTo(previousPoint.x, previousPoint.y);
    context.lineTo(currentPoint.x, currentPoint.y);

    context.stroke();
    context.closePath();

    previousPoint = currentPoint;
  }

  function onMouseEnter({ pageX, pageY }) {
    previousPoint.x = pageX;
    previousPoint.y = pageY;
  }

  function run() {
    canvas.addEventListener('mousemove', onMouseMove)
    canvas.addEventListener('mouseenter', onMouseEnter)
  }

  run()
})();
