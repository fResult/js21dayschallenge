(() => {
  // 1. Class vs Prototype
  class Person {

  }

  const sila = new Person();
  console.log(sila);

  // 2. What's prototype?
  const name = 'Sila';
  console.log(name.__proto__);

  const arr = [];
  console.log(arr.__proto__);

  // 3. Prototype chain
  const pet = 'Cat';
  console.log(pet.__proto__);
  /** toLocalString() is a method from Object prototype
   *  Object is parent class of String
   */
  console.log(pet.toLocaleString())

  // 4. Extend a prototype
  const greeter = 'greeter';
  function sayHello(you) {
    return 'Hello ' + you;
  }
  String.prototype.sayHello = sayHello;

  console.log(greeter.__proto__);
  console.log(greeter.sayHello('Korn'));
})();
