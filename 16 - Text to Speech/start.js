(() => {
  const message = new SpeechSynthesisUtterance();

  function onVoicesChanged() {
    const voices = speechSynthesis.getVoices();
    message.voice = voices.find(voice => voice.lang === 'en-US');
  }

  function onClick(e) {
    message.text = e.target.getAttribute('alt');
    speechSynthesis.speak(message);
  }

  function run() {
    speechSynthesis.addEventListener('voiceschanged', onVoicesChanged);

    const imgElems = Array.from(document.querySelectorAll('img'));
    imgElems.forEach(imgElem => imgElem.addEventListener('click', onClick))
  }

  run();
})();
