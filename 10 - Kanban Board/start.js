(() => {
  let draggingElem;

  function onDragStart() {
    draggingElem = this;
  }

  function onDragEnter(e) {
    e.preventDefault();
  }
  function onDragOver(e) {
    e.preventDefault();
  }

  function onDrop() {
    this.appendChild(draggingElem);
  }

  function run() {
    const taskElems = Array.from(document.querySelectorAll('.task'));
    const dropZoneElems = Array.from(document.querySelectorAll('.drop-zone'));

    taskElems.forEach(taskElem => {
      taskElem.addEventListener('dragstart', onDragStart);
    });

    dropZoneElems.forEach(dropZoneElem => {
      dropZoneElem.addEventListener('drop', onDrop);
      dropZoneElem.addEventListener('dragenter', onDragEnter);
      dropZoneElem.addEventListener('dragover', onDragOver);
    });
  }

  run();
})();
