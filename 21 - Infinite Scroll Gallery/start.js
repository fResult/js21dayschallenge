(async () => {
  const KEY = 'LFk4S2qCi6TMcsgZAC1QyAJcC8lv8BG8bM1yURGKUfM';
  const loaderElem = document.querySelector('.loader');
  let page = 1;

  function showLoader() {
    loaderElem.classList.add('visible');
  }

  function hideLoader() {
    loaderElem.classList.remove('visible');
  }

  async function displayImages() {
    showLoader();

    const result = await fetch(`https://api.unsplash.com/photos/?client_id=${KEY}&page=${page++}`);
    const images = await result.json();

    const galleryElem = document.querySelector('.gallery');

    images.forEach(image => {
      const imageElem = document.createElement('img');
      imageElem.src = image.urls.small;
      galleryElem.appendChild(imageElem);
    });

    hideLoader();
  }

  async function onScroll() {
    const { scrollTop, clientHeight, scrollHeight } = document.documentElement;
    if (scrollTop + clientHeight >= scrollHeight - 10) {
      await displayImages()
    }
  }

  async function run() {
    document.onscroll = onScroll;
    await displayImages();
  }

  await run()
})();
