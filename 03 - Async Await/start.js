(() => {
  // 1. How Asynchronous code works in JavaScript
  function simulateAsyncAPI(text, timeout) {
    setTimeout(() => console.log(text), timeout);
  }

  simulateAsyncAPI('A', 1000);
  simulateAsyncAPI('B', 500);
  simulateAsyncAPI('C', 100);

  // 2. Callback
  function simulateAsyncAPI(text, timeout, callback) {
    setTimeout(() => {
      console.log(text);
      callback && callback();
    }, timeout)
  }

  // 2.5 Callback Hell
  simulateAsyncAPI('A', 1000, () => {
    simulateAsyncAPI('B', 500, () => {
      simulateAsyncAPI('C', 100)
    })
  });

  // 3. Promise
  function simulateAsyncAPI(text, timeout) {
    return new Promise((resolve, reject) => {
      text === 'B' ? reject('B has rejected') : resolve(text);
    })
  }

  simulateAsyncAPI('A', 1000)
    .then((result) => {
      console.log(result);
      return simulateAsyncAPI('B', 500)
    })
    .then((result) => {
      console.log(result);
      return simulateAsyncAPI('C', 100)
    })
    .then(console.log)
    .catch(console.error);

  // 4. Async/Await
  function simulateAsyncAPI(text, timeout) {
    return new Promise((resolve, reject) => {
      text === 'B' ? reject('B has rejected') : resolve(text);
    })
  }

  async function run() {
    try {
      console.log(await simulateAsyncAPI('A', 1000));
    } catch (err) {
      console.error(err)
    }
    try {
      console.log(await simulateAsyncAPI('B', 500));
    } catch (err) {
      console.error(err)
    }
    try {
      console.log(await simulateAsyncAPI('C', 100));
    } catch (err) {
      console.error(err)
    }
  }

  run();

})();
