(() => {
  // 1. NaN
  console.log(NaN * 1 === NaN ? 'Equal' :  'Not equal');

  const result = 1 / 'hello'; // NaN
  if (isNaN(result)) {
    console.log('Equal to NaN')
  }

  // 2. Type Coercion
  if (3 > 2 > 1) {
    console.log('No print coz Type Coercion makes it become falsy');
  }

  console.log(2 - '1'); // 1
  console.log(2 + Number.parseInt('1', 10)); // 3
  console.log(true + true) // 2

  // 3. Interpreter & Compiler
  function getPerson() {
    return {
      name: 'Korn'
    }
  }

  console.log(getPerson());

  // 4. Checking Object Type
  const person = null;
  if (typeof person === 'object' && person !== null) {
    console.log('Yes, Object');
  } else {
    console.log('No, it is not Object');
  }
})();
