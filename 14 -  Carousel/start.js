(() => {
  let currentIndex = 0;

  function displayImage(imageElems, newIndex) {
    const lastIndex = imageElems.length - 1;
    (newIndex < 0)
      ? newIndex = lastIndex
      : (newIndex > lastIndex)
      ? newIndex = 0
      : undefined;
    const imageElem = imageElems[newIndex];
    imageElem.scrollIntoView({ behavior: 'smooth' });

    currentIndex = newIndex;
  }

  function run() {
    const imageElems = document.querySelectorAll('img');
    const previousElem = document.querySelector('.previous');
    const nextElem = document.querySelector('.next');

    previousElem.addEventListener('click', () => displayImage(imageElems, currentIndex - 1));
    nextElem.addEventListener('click', () => displayImage(imageElems, currentIndex + 1));
  }

  run();
})();
