(() => {
  const carBrands = [
    'BMW',
    'Maserati',
    'Mercedes Benz',
    'Ferrari',
    'Lamborghini',
    'Toyota',
    'Honda',
    'Hyundai'
  ];

  const searchElem = document.querySelector('.search');

  function clearResults() {
    const ulElems = Array.from(document.querySelectorAll('.results'));
    ulElems.forEach(ulElem => {
      if(ulElem) document.body.removeChild(ulElem);
    })
  }

  function selectCarBrand(e) {
    searchElem.value = e.target.innerHTML;
    clearResults();
  }

  function onInput(e) {
    clearResults();
    const inputText = e.target.value.toLowerCase();
    const matchedCarBrands = carBrands.filter(carBrand => (
      carBrand.toLowerCase().startsWith(inputText)
    ));

    const ulElem = document.createElement('ul');
    ulElem.classList.add('results');

    matchedCarBrands.forEach(carBrand => {
      const liElem = document.createElement('li');
      liElem.innerHTML = carBrand;
      liElem.onclick = selectCarBrand;
      ulElem.appendChild(liElem);
    });
    document.body.appendChild(ulElem)
  }

  function run() {
    searchElem.oninput = onInput;
    document.onclick = clearResults;
  }

  run();
})();
